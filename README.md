# Spume.co - Avaliação Data Science

Este documento visa explicar e orientar os candidados a vaga de Desenvolvedor/Cientista de dados na Spume.co

## Desafio

Encontrar Insights relevantes em cruzamento de dados entre uma base histórica de visualização de filmes 
extraída da internet com a repercussão nas mídias sociais. Após colher os Insights o candidato deverá montar e entregar um relatório DOC contendo as análises e Insigths sobre a base.

### Artefatos de dados
 - Base histórica de Vizualizações de filmes diversos (CSV)
 - Base extraída do Twitter de menções do filme AVENGERS com métricas (JSON/Dicionary)

### Análise de dados
Deve-se carregar e processar os dados para responder as seguintes questões (Lembre que bases de dados reais podem vir com problemas):

 - Análise descritiva da base histórica dos dados de filmes, agrupar por titulo do filme.
 - Análise descritiva De likes e shares dos tweets do filme AVENGERS
 - Análise comparativa de tendência de Sucesso comparando 3 franquias: 007, Star Wars e Avengers. Utilizar regressão linear ou qualquer outro algortimo de sua escolha caso este possua uma curva de tendência mais Explicativa considerando o aprendizado.
 - Responder se houve ou nao boa repercussão Social do filme AVENGERS
 - Escrever uma conclusão livre considerando a comparação dos títulos e a repercussão do AVENGERS

### Desenvolvimento
O cadidato deve abrir um fork do repositório publicamente e fazer pull request. Após criado deve submeter todo o código gerado para avaliação. As linguagens podem ser R, Python ou NodeJS

### Conclusão
Lembre que além do resultado trazido iremos avaliar estrutura de código , comentários, lógica envolvida e conhecimento de biliotecas utilizadas. É permitido o uso de Bibliotecas de terceiros, porém utilize com moderação.

```python
print( "Obrigado por particiar do nosso desafio !" )
```

### end